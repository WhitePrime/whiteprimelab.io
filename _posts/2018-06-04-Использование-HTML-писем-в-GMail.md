---
layout: post
title: Использование HTML писем в GMail 
categories: Web-service
tags: gmail html
img: https://img.tglab.uz/129366914/15281306335b156c49802fb.jpg
---


Вашему emal'у можно придать уникальности, используя HTML. Это используют множество компаний, для рассылок и часто повторяющихся тематических писем (например при ответе соискателю на вакансию). И необязательно для этого использовать какие-то сервисы рассылок, достаточно и GMail.

#### Интегрируем шаблон HTML в Gmail.

Откройте скачанный пакет шаблона на вашем компьютере. Здесь Вы увидите файл index.html и папку "images" с демо-изображениями.

Откройте файл index.html в любом редакторе кода и выберите все содержимое этого файла, используя сочетание клавиш CTRL+A для Windows (Control+A или Command+A для Mac).

Скопируйте выбранное содержимое в буфер обмена (используя сочетание клавиш CTRL+C для Windows или Control+C/Command+C для Mac).

Войдите в вашу учетную запись Gmail и нажмите на кнопку Создать (Compose), для того чтобы создать новое сообщение.

В открывшемся окне введите какой-нибудь тестовый текст, например, "**************".   
 Нажмите на ваш тестовый текст правой кнопкой мышки и выберите Исследовать элемент (Inspect Element) или Исследовать элемент с Firebug (Inspect Element with Firebug) из контекстуального меню. Затем, в открывшемся окне исследования элемента, найдите введённый текст и замените его скопированным вами кодом HTML (удалите тестовый текст и вставьте скопированный код HTML). 

![](https://img.tglab.uz/129366914/15281304745b156baa826e2.jpg)

![](https://img.tglab.uz/129366914/15281305215b156bd944d77.jpg)

Закройте ваше окно Инструмента исследования, и в разделе нового сообщения Вы увидите ваш шаблон HTML со всеми стилями. Теперь Вы можете указать тему вашего сообщения и отправить его.

Обратит внимание: Для того чтобы видеть изображения вашего шаблона HTML в Gmail, нужно сначала загрузить все изображения на сервер и, затем, в файле index.html заменить все относительные ссылки на изображения (например, images/pattern01.jpg) абсолютными ссылками (например, http://website.test/images/pattern01.jpg), которые включают прямой путь к нужному изображению на вашем сервере.

Вы можете также сохранить сообщение HTML как шаблон электронной почты в Gmail и, затем, использовать его в любое время.

#### Ресурсы с шаблонами HTML писем.


[Salted](https://github.com/rodriguezcommaj/salted)


[Cerberus](http://tedgoas.github.io/Cerberus/)


[Responsive HTML Email Template](https://github.com/leemunroe/responsive-html-email-template)


[Email Blueprints](https://github.com/mailchimp/Email-Blueprints)


[Respmail](http://www.charlesmudy.com/respmail/)


[Ink](https://github.com/zurb/ink)


[Transactional Email Templates](https://github.com/mailgun/transactional-email-templates)


[Antwort](https://github.com/InterNations/antwort)


[Responsive Email](https://github.com/philwareham/responsive-email)



[Responsive Email Patterns](http://briangraves.github.io/ResponsiveEmailPatterns/)


[Zenith](https://github.com/Omgitsonlyalex/ZenithFramework)


[Shopify HTML Email Templates](https://github.com/Cam/Shopify-HTML-Email-Templates)



[Responsive HTML email templates](https://github.com/konsav/email-templates)



[Responsive Email](https://github.com/derekpunsalan/responsive-email)


[WDW](https://github.com/web-design-weekly/WDW-Email-Template)


[Gleemail](https://github.com/groupon/gleemail)



[Postmark Templates](https://github.com/wildbit/postmark-templates)


[EightMedia boilerplate](https://github.com/EightMedia/eightmailboilerplate)





