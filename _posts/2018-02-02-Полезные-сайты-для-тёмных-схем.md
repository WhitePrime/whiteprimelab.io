---
layout: post
title: Полезные сайты для тёмных схем
categories: Web-service
tags: подборка сайты схемы
img: https://img.tglab.uz/129366914/15175851095a7482d559639.jpg
--- 

**Список не полный и будет дополняться.**  


##### Сервисы виртуальных номеров для приема СМС  

onlinesim.ru  
sms-reg.com  
www.receive-sms-online.info  

##### Пробив людей.  
  
phonenumber.to - по номеру телефона, всем известный справочник  
telkniga.com - вбиваем ФИО для получения адреса прописки  
telpoisk.com - еще один справочник  
nomer.org - и еще один 

services.fms.gov.ru/info-service.htm?sid=2000 - проверка валидности паспорта (подойдет для сканов, например)  
service.nalog.ru/inn.do - узнаем номер ИНН  
nomer.org/mosgibdd/ - хорошая, но редко обновляемая база по автовладельцам  
whois.com/whois/ - пробив по IP-адресу  
karta-banka.ru - узнаем банк по номеру карты

Как еще можно пробить человека по номеру телефона через соц.сети - смотрите здесь.

##### Временные почтовые ящики   

www.crazymailing.com/ru/  
temp-mail.ru  
@temp_mail_bot (бот для tlgr)  

##### Самоликвидируюищеся записки 

privnote.com   
pastebin.com    
cryptorffquolzz6.onion  
deaddropinli5cme.onion (работает только с PGP ключом)  

##### VPN-сервисы (которых сейчас полно, приведем только те, что использовали)  
  
nordvpn.com  
hidemy.name  
multivpnwbercchz.onion.link  

##### Сервисы с документами/сканами/дропами и прочим  

dublikat.io  
etc-shop.biz  
  
Таких сервисов просто пруд пруди сейчас. Те, что выше, использованы лично. 

##### Кредиты в битках 

btcjam.com  
bitlendingclub.com  
nebeus.com  

##### Финансовые операции / Документы  

Рекомендую по данным вопросам (обнал, обмен, производство документов и прочая чернуха), небезызвестный [RuTor](https://rutorzzmfflzllk5.onion.link/) не буду давать ссылок на конкретные шопы, разберетесь сами, если не заходили сюда прежде.



