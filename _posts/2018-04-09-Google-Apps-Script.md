---
layout: post
title: Google Apps Script
categories: Web-service
tags: google автоматизация
img: https://img.tglab.uz/129366914/15230296585ac7969a1a1e2.jpg
---

Далеко не все пользователи знают, что в рамках веб-приложений компании Google существует мощный инструмент для автоматизации работы под названием Google Apps Script. 

**Google Apps Script** — это язык программирования, основанный на JavaScript. С его помощью можно добавить дополнительные функции в Google Docs, Google Sheets и Google Forms, а также обрабатывать данные в Google Sheets. Более того, с помощью этого языка можно даже создавать свои приложения, которые будут взаимодействовать не только с продуктами Google, но и с другими онлайновыми сервисами. 

Разработчики найдут всю интересующую их информацию на специальных [справочных страницах](https://developers.google.com/apps-script/quickstart/macros).

#### Gmail 

[Mail Merge with Gmail](http://www.labnol.org/internet/personalized-mail-merge-in-gmail/). Рассылка персонализированных сообщений по электронной почте своим контактам с помощью расширения [Gmail Merge](https://chrome.google.com/webstore/detail/mail-merge-with-attachmen/nifmcbjailaccmombpjjpijjbfoicppp).

[Gmail Autoresponder](http://www.labnol.org/internet/gmail-auto-email-responder/28980/). Ответ на одно или несколько писем в Gmail с помощью предварительно созданных шаблонов.

[Gmail NoResponse](http://jonathan-kim.com/2013/Gmail-No-Response/). Отслеживание писем в Gmail, на которые вы не получили ответа. Пригодится, чтобы ещё раз напомнить о себе.

[Gmail Delay Send](http://googleappsdeveloper.blogspot.in/2012/05/email-overload.html). Этот скрипт является простым, удобным и безопасным способом запланировать время отправки писем в Gmail.

[Gmail Snooze](http://googleappsdeveloper.blogspot.in/2011/07/gmail-snooze-with-apps-script.html). Теперь у вас появится возможность отложить письмо на некоторое время. В результате оно спрячется из папки «Входящие», но вновь появится там в назначенный час.

[Gmail Auto Purge](http://www.labnol.org/internet/gmail-auto-purge/27605/). Работа этого скрипта похожа на существующую функцию Outlook, которая автоматически удаляет старые сообщения от определённых пользователей через указанное вами время.

[Gmail Clean-up](https://www.johneday.com/422/time-based-gmail-filters-with-google-apps-script). Создание автоматических фильтров в Gmail, которые умеют в назначенное время перемещать, архивировать и даже удалять все сообщения, отмеченные определённым ярлыком.

[Save Gmail as PDF](http://ctrlq.org/code/19117-save-gmail-as-pdf). Этот скрипт позволяет сохранять текст писем в виде PDF-файла. Вы также сможете отправлять эти файлы своим почтовым контактам.

[SMS Alerts for Gmail](http://www.labnol.org/internet/twitter-as-gmail-notifier-sms/20990/). Получение уведомления по SMS о приходе важных сообщений в Gmail с помощью соединения почтового ящика с личным аккаунтом Twitter.

[Extract Email Addresses](http://www.labnol.org/internet/extract-gmail-addresses/28037/). Этот скрипт анализирует содержимое вашего почтового ящика и создаёт список адресов людей, которые ранее общались с вами. Пригодится для начала личной рекламной кампании.

[Transfer Gmail](http://www.labnol.org/internet/transfer-gmail/28088/). Этот скрипт автоматически скопирует все ваши письма со старого аккаунта Gmail в другой почтовый ящик, который может быть на любом сервисе.

[Reminder for Starred Messages](http://www.labnol.org/internet/gmail-starred-emails/27691/). Получение ежедневного отчёта со списком всех писем, которые вы недавно пометили звёздочкой в Gmail.

[Advanced Gmail Search](http://www.labnol.org/internet/advanced-gmail-search/21623/). Gmail поддерживает большое количество команд поиска, однако теперь вы сможете использовать регулярные выражения для поиска нужных писем в своём почтовом ящике.

[Save Gmail Images](http://www.labnol.org/internet/save-gmail-attachments-to-google-drive/28008/). Этот скрипт отслеживает ваш почтовый ящик Gmail и автоматически сохраняет любую картинку из вложений в Google Drive.

[Sort Gmail by Size](http://www.labnol.org/internet/sort-gmail-by-size/). Пригодится, когда заканчивается место в Gmail. Этот скрипт позволяет найти самые большие сообщения в вашем почтовом аккаунте.

[Bulk Forward Gmail](http://www.labnol.org/internet/auto-forward-gmail-messages/20665/). Автоматическая пересылка работает обычно только с новыми сообщениями, поступающими в Gmail. Но с помощью этого скрипта вы сможете организовать массовую пересылку даже старых сообщений.

[Gmail Encrypt](http://www.labnol.org/internet/encrypt-gmail/28191/). Вы можете шифровать ваши письма в Gmail с использованием мощных алгоритмов AES-шифрования, и никто не сможет перехватить вашу переписку.

[Schedule Gmail Emails](http://www.labnol.org/internet/schedule-gmail-send-later/24867/). Напишите письмо и отошлите его в любое назначенное вами время с помощью Apps Script и Google Sheets.

[Gmail Label Feeder](http://mashe.hawksey.info/2013/05/gmail-label-to-rss-feed/). Создание RSS-канала для любого ярлыка вашего почтового ящика Gmail, который вы потом сможете направить в Evernote, Pocket, IFTTT и так далее.

[Gmail Unsubscriber](http://www.labnol.org/internet/gmail-unsubscribe/28806/). Автоматическая отписка от списков рассылки и рекламных сообщений.

#### GDrive 

[Auto-Expire Shared Folders](http://www.labnol.org/internet/auto-expire-google-drive-links/27509/). Возможность назначить срок, по истечении которого пользователи потеряют доступ к расшаренным ранее папкам в Google Drive.

[Instagram Downloads](https://sites.google.com/site/appsscripttutorial/home/downloading-instagram-photos-to-your-google-drive-using-google-apps-script). Загрузка фотографий, отмеченных определёнными тегами, из Instagram на ваш Google Drive.

[GDocs2MD](https://github.com/mangini/gdocs2md). Этот скрипт конвертирует документы из Google Drive в популярный формат Markdown (.md) , что даёт возможность их импорта и публикации на других платформах.

[Send to Google Drive](http://www.labnol.org/internet/send-gmail-to-google-drive/21236/). Сохранение вложений из Gmail непосредственно в Google Drive с помощью расширения [Save Emails](https://chrome.google.com/webstore/detail/save-emails-and-attachmen/nflmnfjphdbeagnilbihcodcophecebc).

[Files Permissions Explorer](http://www.labnol.org/internet/google-drive-access/28237/). Просматривайте аккаунты, которые имеют доступ к вашим расшаренным файлам в Google Drive с возможностью изменения прав доступа.

[1-click Website Hosting](http://www.labnol.org/internet/host-website-on-google-drive/28178/). Этот Google Script поможет организовать хостинг сайтов, изображений, подкастов и других файлов в Google Drive в один клик.




#### Other 

[Google Form File Uploads](http://www.labnol.org/internet/receive-files-in-google-drive/19697/). С помощью этой формы вы сможете получать файлы непосредственно в Google Drive с любой страницы, в которую она будет встроена.

[Website Uptime Monitor](http://www.labnol.org/internet/website-uptime-monitor/21060/). Получение оповещения по электронной почте и SMS, если ваш сайт недоступен. Вы можете бесплатно мониторить любой сайт в Сети.

[Send Google Spreadsheets as PDF](http://ctrlq.org/code/19869-email-google-spreadsheets-pdf). Конвертирование ваших таблиц в формат PDF и отправка указанным контактам по расписанию.

[Sell Digital Products Online](http://www.labnol.org/internet/sell-digital-products-online/28554/). Используйте связку PayPal и Google Drive, чтобы запустить собственный цифровой магазин онлайн.

[Gravity Forms to Google Sheets](http://ctrlq.org/code/20047-gravity-forms-to-google-spreadsheet). Запустите Google Script, который будет сохранять ваши значения из формы Gravity WordPress в Google Spreadsheet.

[Google Web Scraping](http://www.labnol.org/internet/google-web-scraping/28450/). Импорт результатов поиска Google в Google Spreadsheet с помощью функции ImportXML для анализа.

[Update Google Contacts](http://www.labnol.org/internet/google-contacts-updated/27306/). Этот скрипт даёт возможность вашим друзьям и членам семьи обновить свою контактную информацию непосредственно в вашей адресной книге Google.

[Google Contacts Map](http://www.labnol.org/internet/google-contacts-map/26107/). Просмотрите расположение всех своих контактов из адресной книги Google на карте. Есть возможность экспортировать данные в формате KML для просмотра в программе Google Earth.

[Email Form Data](http://www.labnol.org/internet/google-docs-email-form/20884/). Google Forms — лучший инструмент для создания онлайновых опросов и розыгрышей. Этот скрипт отправляет вам данные, как только кто-то заполняет вашу форму.

[Auto Confirmation Emails](http://www.labnol.org/internet/auto-confirmation-emails/28386/). Отправка подтверждающего письма после заполнения пользователем Google Forms.

[Schedule Google Forms](http://www.labnol.org/internet/schedule-google-forms/20707/). Установка срока действия для Google Forms, что автоматически закроет опрос в назначенное время.

[Translate RSS](http://www.labnol.org/internet/google-translate-rss-feeds/). Возможность автоматического перевода иностранных RSS-каналов на родной язык и подписка на них с помощью любимой RSS-программы или сервиса.

[Text Browser](http://www.labnol.org/internet/google-text-browser/26553/). Lynx-подобный браузер, который позволяет сёрфить по интернету в текстовом виде. Этот браузер также может быть использован в качестве прокси-сервера для доступа к веб-контенту.

[Self-destructive Messages](http://www.labnol.org/internet/send-self-destructing-messages/26125/). Отправляйте конфиденциальные сообщения, которые автоматически исчезнут после их прочтения.

[Reddit Scraper](http://www.labnol.org/internet/web-scraping-reddit/28369/). Используйте Reddit API вместе с Google Scripts, чтобы загрузить все посты с любого топика Reddit прямо в таблицу Google.
