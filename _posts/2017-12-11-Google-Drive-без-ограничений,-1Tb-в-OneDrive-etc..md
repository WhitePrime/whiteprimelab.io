---
layout: post
title: Google Drive без ограничений, 1Tb в OneDrive etc.
categories: Web-service
tags: облако лайфхак
img: https://tlgur.com/d/KG7mllE4
---
﻿


В данной сегодняшней теме речь пойдет о том как получить эти (а может кому интересны) и другие плюшки абсолютно бесплатно потратив на это от силы 1-2 часа. 

----- 

Для этого нам понадобится почта в зоне .edu, которая предоставляется совершенно бесплатно её пользователям. В Америке она нужна для студентов, но мы то с вами тоже хотим поиметь с этого кусок. По сути тот же Gmail, но намного вкуснее.


Прежде чем приступить я перечислю некоторые сервисы, которые благодаря этой почте вы сможете использовать совершенно бесплатно, либо же с большущей скидкой. Подчеркну те, которые лично для меня являются важнейшими.

Основные сервисы:

Предоставляется Office 365 бесплатно вместе с 1 ТБ места в облаке OneDrive (бывают траблы с приходом кода на почту)
Unlimited Google Drive (гарантировано)
Adobe Creative Cloud скидка в размере 60%
Apple Education Store экономия $200 на новый Мак, $20 на новый iPad
Amazon Prime Student бесплатный премиальный личный кабинет который предоставляет бесплатную 2х дневную доставку и скидки (не работает с большинством РУ карт)
Microsoft DreamSpark акция Microsoft, предоставляющая бесплатный доступ к программам разработки ПО и другим программам (такие же траблы как и с офисом)
Autodesk Education набор бесплатных программ куда входит AutoCAD, 3ds Max и др.
Другие сервисы:

Amazon Web Service
Mindsumo
JetBrains
Riptiger software
Solid edge student edition software
Lucidchart
Fetch
Prezi
Bitbucket cloud
Free .me domain name from Namecheap (не у всех выходило)
6 month Lastpass premium password manager
GitHub student dev pack
Jetbrains
Axure
PTC Creo 3D software
ynab
1-year newegg premier service (qualifying schools only)
Roboform
Spotify 50% off membership
iDrive 50% off online backup
10% off on Asos
Колледж где нам дадут .edu http://home.cccapply.org/ (настаиваю, что стоит всё делать через VPN USA).

Выбрать в выпадающим меню Canada College.

Регистрация в Open CCC http://my.smccd.edu

НЕ ЗАБЫВАЙТЕ! Заполнять форму нужно с указанием Калифорнийского адреса (СА) .

Генерация реального адреса в Калифорнии http://www.fakenamegenerator.com/

Валидация SNN http://www.ssnregistry.org/validate (вместо -ХХХХ, любые 4 цифры)


Как я и говорил до этого лучше всё исполнять из под VPN/дедика USA (амазоновский вполне пойдет)

1. Идём на сайт и выбираем Canada Colledge. Жмем APPLY

![](https://pp.userapi.com/c638616/v638616674/5f8e6/lNhMSYIt8FU.jpg)

2. На открывшейся странице выбираем Complete...

![](https://pp.userapi.com/c638616/v638616674/5f8f0/ivAkGRUhpDQ.jpg)

3. На открывшейся странице выбираем "Create an Account"

![](https://pp.userapi.com/c638616/v638616674/5f8fa/kiMsiStgTEM.jpg)

4. Жмем на Begin..

![](https://pp.userapi.com/c638616/v638616674/5f904/5fMrGlzaSyU.jpg)

5. Идем на сайт и генерируем пока не получим штат California (смотреть скрин) (советую также выбирать функцию Advanced Options и выставлять возраст от 18 до 22-23, чтобы приближенно казаться студентом, male/female по желанию, остальное не трогать)

![](https://pp.userapi.com/c638616/v638616674/5f90e/-ry5QcbHhi8.jpg)

6. Идем на сайт и дописываем к нашим получившимся цифрам, как на скрине 5 рандомные четыре цифры, чтобы написало "...is valid for CA"

![](https://pp.userapi.com/c638616/v638616563/54026/2DgKm3j-Y6k.jpg)

7. Заполняем всё как на скриншоте (касается галочек и прочего), но со своими данными. Как раз для графы SSN мы выполняли предыдущий пункт. Жмём "Continue"

![](https://pp.userapi.com/c638616/v638616563/54030/D1gWy8i4Beo.jpg)

8. Расписываем как на скриншоте, почта желательно Gmail, можете свою указать в принципе (я использовал отдельную). Номер также указывайте рандом, прозвона не будет, но советую указывать похожий на реальный, желательно ещё по штату сделать (знаю, что на скриншоте Коннектикут или Нью-Джерси номер)(код можно посмотреть здесь). Лично я на всякий случай указал номер полученный на TextNow. Жмем "Continue"

![](https://pp.userapi.com/c638616/v638616563/5403a/qSQtRpnWN8A.jpg)

9. Придумывайте Username, Password, PIN и ответы на вопросы. На вашем месте я советую записать их куда-нибудь для дальнейшего доступа к кабинету, возможно, в будущем он вам понадобится и жмем "Create My Accaunt"

![](https://pp.userapi.com/c638616/v638616563/54044/0zmJ9Ti2do8.jpg)

10. Также советую записать ваш CCCID на будущее. Жмем "Continue"

![](https://pp.userapi.com/c638616/v638616563/5404e/5TB81MTm-go.jpg)

11. Жмём "Start Application"

![](https://pp.userapi.com/c638616/v638616563/54058/_A3y1NTwdZ8.jpg)

12. В "Term Applying For" у вас будет своя дата, выбирайте её, далее всё как на скриншоте. Жмем "Continue"

![](https://pp.userapi.com/c638616/v638616563/5406c/5edtIlTwIRw.jpg)

13. Ставим галочку в указанном месте и нажимаем "Continue"

![](https://pp.userapi.com/c638616/v638616563/54076/ZicCufX-3b8.jpg)

14. В зависимости от выбранного пола ставите свой, далее следуем странице, нажимая "Continue"

![](https://pp.userapi.com/c638616/v638616563/54080/Ns-I9BvNTG0.jpg)

На этом всё, всем успехов!